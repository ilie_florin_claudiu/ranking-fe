import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { ConfigModel } from './app/models/config.model';
import { APP_CONFIG } from './app/models/constants';

let configURL = environment.production ? './assets/prod/config.json' : './assets/dev/config.json';

fetch(configURL)
    .then((response) => response.json())
    .then((config: ConfigModel) => {
      if (environment.production) {
        enableProdMode();
      }

      platformBrowserDynamic([{ provide: APP_CONFIG, useValue: config }])
          .bootstrapModule(AppModule)
          .catch((err) => console.error(err));
    });
