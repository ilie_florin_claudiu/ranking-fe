import {APP_INITIALIZER, ErrorHandler, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {StoreModule} from "@ngrx/store";
import {metaReducers, reducers} from "./store/reducers";
import {ConfigService} from "./store/services/config.service";
import {EffectsModule} from "@ngrx/effects";
import {AppEffects} from "./store/effects/app-effects";
import {ToastrModule} from "ngx-toastr";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {CarouselModule} from "ngx-owl-carousel-o";
import {FormsModule} from "@angular/forms";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {InfiniteScrollModule} from "ngx-infinite-scroll";
import {HeadersInterceptor} from "./interceptors/HeadersInterceptor";

@NgModule({
  declarations: [
    AppComponent,
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        CarouselModule,
        StoreModule.forRoot(reducers, {
            metaReducers,
        }),
        ToastrModule.forRoot(),
        EffectsModule.forRoot([AppEffects]),
        FormsModule,
        NgbModule,
        InfiniteScrollModule
    ],
  providers: [
    ConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: () => () => {
        return new Promise<void>((resolve) => {
          resolve();
        });
      },
      deps: [ConfigService],
      multi: true,
    },
    { provide: HTTP_INTERCEPTORS, useClass: HeadersInterceptor, multi: true },
    {
      provide: ErrorHandler,
      useClass: ErrorHandler,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
