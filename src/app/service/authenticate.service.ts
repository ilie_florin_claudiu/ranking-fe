import {Injectable} from '@angular/core';
import {ConfigService} from "./config.service";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {
  AuthenticateAccountResult,
  LoginRequest,
  RankingAccountModel,
  RefreshTokenResult
} from "../models/authenticate.model";

@Injectable({
  providedIn: 'root'
})
export class AuthenticateService {

  constructor(private http: HttpClient,
              private configService: ConfigService
  ) {}

  getAccount(account: string): Observable<RankingAccountModel> {
    return this.http.post<RankingAccountModel>(
        `${this.configService.config.BACKEND_URL}auth/getAccount?account=${account}`,
        {}
    );
  }

  authenticateAccount(loginRequest: LoginRequest): Observable<AuthenticateAccountResult> {
    return this.http.post<AuthenticateAccountResult>(
        `${this.configService.config.BACKEND_URL}auth/authenticateAccount`,
        loginRequest
    );
  }


  refreshToken(): Observable<RefreshTokenResult> {
    return this.http.post<RefreshTokenResult>(
        `${this.configService.config.BACKEND_URL}auth/refreshToken`,
        {},
        {
          withCredentials: true
        }
    );
  }
}
