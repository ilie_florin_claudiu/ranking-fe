import { Inject, Injectable } from '@angular/core';
import { ConfigModel } from '../models/config.model';
import { APP_CONFIG } from '../models/constants';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root',
})
export class ConfigService {
  config: ConfigModel;
  abi: any;

  constructor(
      @Inject(APP_CONFIG) private readonly appConfig: ConfigModel,
      private http: HttpClient
  ) {
    this.config = this.appConfig;
  }

  loadAbi() {
    let abi = './assets/dev/abi.json';
    if (environment.production) {
      //abi = './assets/prod/abi.json';
    }
    return this.http
        .get(abi)
        .toPromise()
        .then(config => {
          this.abi = config;
        });
  }
}
