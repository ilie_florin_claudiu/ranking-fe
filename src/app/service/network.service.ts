import {Injectable, NgZone} from '@angular/core';
import Web3 from "web3";
import Web3Modal from "web3modal";
import {ConfigService} from "./config.service";
import {Store} from "@ngrx/store";
import WalletConnectProvider from "@walletconnect/web3-provider";
import {
  connectFailed, fetchAccountDataRequest
} from "../store/actions/actions";
import {HttpClient} from "@angular/common/http";
import {AuthenticateService} from "./authenticate.service";
import {concat, concatMap, of, zip} from "rxjs";
import {AuthenticateAccountResult, RankingAccountModel, RefreshTokenResult} from "../models/authenticate.model";
import {map} from "rxjs/operators";

declare let window:any;

@Injectable({
  providedIn: 'root'
})
export class NetworkService {
  web3Modal;
  smartContract: any;
  ethereumProvider: any;

  constructor(
      private zone:NgZone,
      private store: Store<any>,
      private configService: ConfigService,
      private authenticateService: AuthenticateService,
      private http: HttpClient
  ) {

    const providerOptions = {
      walletconnect: {
        package: WalletConnectProvider, // required
        options: {
          infuraId: "eb3ef95df4464cf6bce388dd661ab7ec" // required
        }
      }
    };

    this.web3Modal = new Web3Modal({
      cacheProvider: false, // optional
      providerOptions,
      theme: {
        background: "rgb(39, 49, 56)",
        main: "rgb(199, 199, 199)",
        secondary: "rgb(136, 136, 136)",
        border: "rgba(195, 195, 195, 0.14)",
        hover: "rgb(16, 26, 32)"
      }
    });
  }


  async connect() {
    try {
      this.ethereumProvider = await this.web3Modal.connect(); // set provider
      let web3js = new Web3(this.ethereumProvider); // create web3 instance

      if (this.ethereumProvider) {
        const CONFIG = this.configService.config;

        try {

          const accounts = await web3js.eth.getAccounts();
          const networkId = await web3js.eth.net.getId();

          if (networkId == CONFIG.NETWORK_ID) {
            this.ethereumProvider.on('accountsChanged', (accounts: string[]) => {
              this.zone.run(() => { // <== added
                this.connectSuccess(accounts[0]);
              });
            });
            this.ethereumProvider.on('chainChanged', () => {
              window.location.reload();
            });
            this.connectSuccess(accounts[0]);
          } else {
            await this.onConnectError(`change network to ${CONFIG.NETWORK_NAME}.`);
          }
        } catch (err) {
          await this.onConnectError(err);
        }
      } else {
        await this.onConnectError('ethereum provider could not be found. install metamask or use wallet connect.');
      }

    } catch (err) {
      await this.onConnectError('ethereum provider could not be found. install metamask or use wallet connect.');
    }

  }


  async loadContract(contractId: string) {
    try {

      if (this.ethereumProvider) {
        let web3js = new Web3(this.ethereumProvider); // create web3 instance
        if (!this.configService.abi) {
          await this.configService.loadAbi();
        }

        const ABI = this.configService.abi;

        this.smartContract = new web3js.eth.Contract(ABI, contractId)

        //this.store.dispatch(fetchContractDataRequest());
      } else {
        await this.onConnectError('please connect first to metamask.');
      }

    } catch (err) {
      await this.onConnectError(err);
    }

  }

  private async onConnectError(errorMessage: string) {
    await this.disconnect();

    this.store.dispatch(connectFailed({errorMessage}));
  }

  private async disconnect() {
    //for wallet connect, we use disconnect
    if (this.ethereumProvider && !this.ethereumProvider.isMetaMask) {
      await this.ethereumProvider.close();
    }

  }

  async connectSuccess(account: string) {

    let accountResult = await this.authenticateService.getAccount(account).toPromise();
    let authenticateAccountResult = await this.authenticateService.authenticateAccount({
      address: accountResult!.account,
      signature: accountResult!.nonce
    }).toPromise();

    let refreshTokenResult = await this.authenticateService.refreshToken().toPromise();

    localStorage.setItem("token", authenticateAccountResult!.authenticateAccount.accessToken);

    this.store.dispatch(fetchAccountDataRequest({account}));
  }


}
