import { Injectable } from '@angular/core';
import {ConfigService} from "./config.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {catchError, Observable, of} from "rxjs";
import {ContractDataModel, OpenSeaNftModel, PaginatedResultModel} from "../models/collection.model";
import {defaultPaginationModel, PaginationQueryModel} from "../models/pagination-query.model";

@Injectable({
  providedIn: 'root'
})
export class OpenseaService {

  constructor(private http: HttpClient, private configService: ConfigService) {}

  getOpenseaDetails(contractAddress: string, tokenId: number): Observable<OpenSeaNftModel> {
    let headers = new HttpHeaders();
    headers.append("Accept", "application/json");

    return this.http.get<OpenSeaNftModel>(
        `${this.configService.config.OPENSEA_URL}asset/${contractAddress}/${tokenId}`,
        {headers: headers}
    );
  }
}
