import { Injectable } from '@angular/core';
import {ConfigService} from "./config.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {catchError, Observable, of} from "rxjs";
import {
  ContractDataModel, ContractModel, ContractSearchResult,
  OpenSeaNftModel,
  PaginatedResultModel
} from "../models/collection.model";
import {
  defaultPaginationModel,
  defaultStandardPaginationModel,
  PaginatedModel,
  PaginationQueryModel
} from "../models/pagination-query.model";

@Injectable({
  providedIn: 'root'
})
export class ContractService {

  constructor(private http: HttpClient, private configService: ConfigService) {}

  getContractData(contractAddress: string): Observable<ContractDataModel> {
    return this.http.get<ContractDataModel>(
        `${this.configService.config.BACKEND_URL}api/rank/${contractAddress}`
    );
  }

  getContractsPaginated(
      query?: PaginationQueryModel
  ): Observable<PaginatedModel<ContractModel[]>> {
    let queryString = query ? `?${query.getQueryString()}` : '';
    return this.http
        .get<PaginatedModel<ContractModel[]>>(
            `${this.configService.config.BACKEND_URL}contract${queryString}`
        )
        .pipe(catchError(() => of(defaultStandardPaginationModel())));
  }


  getContractDataPaginated(
      contractAddress: string,
      force: boolean,
      query?: PaginationQueryModel
  ): Observable<PaginatedResultModel> {
    let queryString = query ? `?${query.getQueryString()}` : '';
    if (force) {
      queryString += `${queryString ? '&' : '?'}forceOpensea=${force}`;
    }
    return this.http
        .get<PaginatedResultModel>(
            `${this.configService.config.BACKEND_URL}contract/assets/${contractAddress}${queryString}`
        )
        .pipe(catchError(() => of(defaultPaginationModel())));
  }


  contractDelete(
      contractAddress: string
  ): void{
    this.http
        .delete(
            `${this.configService.config.BACKEND_URL}contract/${contractAddress}`,
            {
              withCredentials: true
            }
        ).subscribe();

  }

  contractMonitor(
      contractAddress: string
  ): void{
    this.http
        .get(
            `${this.configService.config.BACKEND_URL}contract/monitor/${contractAddress}`
        ).subscribe();

  }

  contractUpdateRank(
      contractAddress: string
  ): void{
    this.http
        .get(
            `${this.configService.config.BACKEND_URL}contract/add-and-rank/${contractAddress}`
        ).subscribe();

  }

  searchContract(contractAddress: string): Observable<ContractSearchResult> {
    return this.http.get<ContractSearchResult>(
        `${this.configService.config.BACKEND_URL}contract/search/${contractAddress}`
    );
  }

  saveContract(contractAddress: string, contractSearchResult: ContractSearchResult): Observable<ContractDataModel> {
    return this.http.post<ContractDataModel>(
        `${this.configService.config.BACKEND_URL}contract/${contractAddress}`,
        contractSearchResult
    );
  }

  rankContract(
      contractAddress: string
  ): void{
    this.http
        .get(
            `${this.configService.config.BACKEND_URL}contract/rank/${contractAddress}`
        ).subscribe();

  }
}
