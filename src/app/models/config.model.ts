export interface ConfigModel {
  NETWORK_NAME: string;
  NETWORK_SYMBOL: string;
  NETWORK_ID: number;
  BACKEND_URL: string;
  OPENSEA_URL: string;

  ENVIRONMENT_ID: Environments;
}

export enum Environments {
  TEST = 'test',
  UAT = 'uat',
  PRODUCTION = 'prod',
}
