export interface ContractDataModel {
  totalSupply: number,
  startTokenId: number,
  startTokenURI: string,
  isRevealed: boolean,
}

export interface ContractSearchResult {
  contractDataModel: ContractDataModel,
  rarityOptions: RarityOptions,
}

export interface RarityOptions {
  excludeNumbers: boolean,
  excludeDates: boolean,
  includeTraitsCount: boolean,
  includeNoneValues: boolean,
  normalizeValues: boolean,
  algorithmType: RarityAlgorithm
}

export interface RankResult {
  tokenId: number;
  name: string;
  description: string;
  forSale: boolean;
  image: string;
  openseaImage: string;
  saleLink: string;
  salePrice: number;
  rank: number;
  rarityScore: number;
  attributes: AttributesModel[];
}

export interface AttributesModel {
  traitType: string;
  value: string;
  count: number;
  percentage: number;
  rarityScore: number;
}



export interface PaginatedResultModel {
  totalSupply: number,
  firstTokenURI: number,
  isRevealed: boolean,
  contractStatus: string,

  data: RankResult[];

  nextPage: number;

  pageNumber: number;

  pageSize: number;

  previousPage: number;

  totalPages: number;

  totalRecords: number;
}

export interface OpenSeaNftModel {
  token_id: number;
  token_metadata: string;
  permalink: string;
  image_thumbnail_url: string;

  sell_orders: SellOrderModel[];
  orders: OrderModel[];
}

export interface SellOrderModel {
  expiration_time: number;
  fee_method: number;
  sale_kind: number;
  side: number;
  maker_relayer_fee: string;
  taker_relayer_fee: string;
  base_price: number;

}

export interface OrderModel {
  expiration_time: number;
  current_price: number;

  fee_method: number;
  sale_kind: number;
  side: number;
  maker_relayer_fee: string;
  taker_relayer_fee: string;

}


export interface ContractModel {
  contractAddress: string,
  name: string,
  status: string,
  monitorUrl: number,
  revealUrl: number,
  rarityOptions: RarityOptions,

  startTokenId: number,
  totalSupply: number,
}


export enum RarityAlgorithm {
  RaritySniper,
  RarityCow,
}
