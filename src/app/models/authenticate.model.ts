export interface RankingAccountModel {
  account: string,
  nonce: number
}

export interface LoginRequest {
  address: string,
  signature: number
}
export interface AuthenticateAccountResult {
  authenticateAccount: RefreshTokenResult
}
export interface RefreshTokenResult {
  success: boolean,
  accessToken: string,
  account: Account,

}
export interface Account {
  membershipType: MembershipType,
  role: Role,
}


export enum Role {
  FREE,
  MEMBER,
  SUBSCRIBER,
  ADMIN
}

export enum MembershipType {
  FOUNDER,
  LIFETIME,
  SUBSCRIPTION
}