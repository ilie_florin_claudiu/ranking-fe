import {InjectionToken} from '@angular/core';
import {ConfigModel} from './config.model';

export const APP_CONFIG: InjectionToken<ConfigModel> = new InjectionToken<ConfigModel>(
  'Application Configuration'
);
