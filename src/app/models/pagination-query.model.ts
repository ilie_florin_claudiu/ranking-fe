export class PaginationQueryModel {
  pageNumber?: number;

  pageSize?: number;

  querySearch?: string;

  constructor() {
    this.reset();
  }

  getQueryString(): string {
    const queryArr = [];

    if (this.pageNumber) {
      queryArr.push(`pageNumber=${this.pageNumber}`);
    }
    if (this.pageSize) {
      queryArr.push(`pageSize=${this.pageSize}`);
    }
    if (this.querySearch) {
      queryArr.push(`querySearch=${this.querySearch}`);
    }

    return queryArr.length ? queryArr.join('&') : '';
  }

  reset(): void {
    this.pageNumber = 0;
    this.pageSize = 250;
  }
}


export const defaultPaginationModel = () => ({
  totalSupply: 0,
  firstTokenURI: 0,
  isRevealed: false,
  nextPage: 0,
  pageNumber: 0,
  pageSize: 0,
  previousPage: 0,
  totalPages: 0,
  totalRecords: 0,
  contractStatus: "",
  data: [],
});

export interface PaginatedModel<Type> {
  data: Type;

  nextPage: number;

  pageNumber: number;

  pageSize: number;

  previousPage: number;

  totalPages: number;

  totalRecords: number;
}

export const defaultStandardPaginationModel = () => ({
  nextPage: 0,
  pageNumber: 0,
  pageSize: 0,
  previousPage: 0,
  totalPages: 0,
  totalRecords: 0,
  contractStatus: "",
  data: [],
});