import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpHeaders } from '@angular/common/http';

@Injectable()
export class HeadersInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        let accessToken = localStorage.getItem("token");
        let headers = {};
        if (!!accessToken) {
            headers = {
                "Authorization": `Bearer ${accessToken}`
            }
        }
        const httpRequest = req.clone({
            headers: new HttpHeaders(headers),
            withCredentials: true
        });

        return next.handle(httpRequest);
    }
}