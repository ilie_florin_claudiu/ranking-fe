import { createAction, props } from '@ngrx/store';
import { ContractDataModel} from "../../models/collection.model";

export const appInit = createAction('APP_INIT');

export const connectRequest = createAction('CONNECTION_REQUEST');


export const connectSuccess = createAction(
    'CONNECTION_SUCCESS',
    props<{ account: any }>()
);

export const connectFailed = createAction(
    'CONNECTION_FAILED',
    props<{ errorMessage: any }>()
);


export const fetchAccountDataRequest = createAction(
    'ACCOUNT_DATA_REQUEST',
    props<{ account: any }>()
);

export const fetchAccountDataSuccess = createAction(
    'ACCOUNT_DATA_SUCCESS',
    props<{ myBalance: any }>()
);

export const fetchAccountDataFailed = createAction(
    'ACCOUNT_DATA_FAILED',
    props<{ errorMessage: any }>()
);

export const fetchContractDataRequest = createAction(
    'CONTRACT_DATA_REQUEST'
);

export const fetchContractDataSuccess = createAction(
    'CONTRACT_DATA_SUCCESS',
    props<ContractDataModel>()
);

export const fetchContractDataFailed = createAction(
    'CONTRACT_DATA_FAILED',
    props<{ errorMessage: any }>()
);


export const getContractListRequest = createAction(
    'CONTRACT_LIST_REQUEST',
    props<{ collectionAddress: string}>()
);

export const getContractListSuccess = createAction(
    'CONTRACT_LIST_SUCCESS',
    props<{ contractData: ContractDataModel }>()
);

export const getContractListFailure = createAction(
    'CONTRACT_LIST_FAILURE',
    props<{ errorMessage: string }>()
);
