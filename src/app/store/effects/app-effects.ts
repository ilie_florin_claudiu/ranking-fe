import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {
  getContractListRequest,
  getContractListSuccess,
  getContractListFailure, connectFailed, fetchContractDataRequest, fetchContractDataFailed, fetchAccountDataFailed
} from '../actions/actions';
import {filter, map, switchMap, tap} from 'rxjs/operators';
import {ToastrService} from "ngx-toastr";
import {Store} from "@ngrx/store";
import {NetworkService} from "../../service/network.service";
import {OpenseaService} from "../../service/opensea.service";
import {ContractDataModel} from "../../models/collection.model";
import {ContractService} from "../../service/contract.service";

@Injectable()
export class AppEffects {

  constructor(private actions$: Actions,
              private openSea: OpenseaService,
              private contractService: ContractService,
              private networkService: NetworkService,
              private toastr: ToastrService,
              private store: Store<any>,
  ) {
  }


  getCollectionRequest$ = createEffect(() => this.actions$.pipe(
      ofType(getContractListRequest),
      switchMap(({collectionAddress}) => this.contractService.getContractData(collectionAddress)),
      map((resp: ContractDataModel) => getContractListSuccess({contractData: resp}))
      )
  );


  errorConnectToast$ = createEffect(() => this.actions$.pipe(
      ofType(connectFailed, fetchAccountDataFailed),
      tap(({errorMessage}) => this.toastr.error(errorMessage))
      ),
      {dispatch: false}
  );

}
