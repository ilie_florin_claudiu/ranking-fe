import {createReducer, on} from '@ngrx/store';
import {
    connectRequest,
    connectSuccess,
    connectFailed,
    fetchAccountDataRequest,
    fetchAccountDataFailed,
    fetchAccountDataSuccess,
    getContractListRequest,
    getContractListSuccess,
    getContractListFailure,
} from '../actions/actions';
import {AccountContractState, CollectionState} from "./index";


const initialAccountContractState : AccountContractState = {
    accountDataLoaded: false,
    loading: false,
    account: null,
    myBalance: 0,
    errorMessage: null,
};

const initialCollectionState : CollectionState = {
    loaded: false,
    errorMessage: null,
    contractData: null,
    loading: false,
}


export const CollectionStateReducer = createReducer(
    initialCollectionState,
    on(getContractListRequest, (state, {}) => ({
        ...state,
        loading: true,
        loaded: false,
        errorMessage: null,
        contractData: null,
    })),
    on(getContractListSuccess, (state, {contractData}) => ({
        ...state,
        loading: false,
        loaded: true,
        errorMessage: null,
        contractData: contractData,
    })),
    on(getContractListFailure, (state, {errorMessage}) => ({
        ...state,
        loading: false,
        loaded: false,
        errorMessage: errorMessage,
        contractData: null,
    })),
);


export const AccountContractStateReducer = createReducer(
    initialAccountContractState,
    on(connectRequest, (state, {}) => ({
        ...state,
        loading: true,
        accountDataLoaded: false,
    })),
    on(connectSuccess, (state, { account }) => ({
        ...state,
        loading: false,
        accountDataLoaded: true,
        account,
    })),
    on(connectFailed, (state, {errorMessage}) => ({
        ...state,
        loading: false,
        accountDataLoaded: false,
        errorMessage,
    })),
    on(fetchAccountDataRequest, (state, {account}) => ({
        ...state,
        account,
        loading: true,
        accountDataLoaded: false,
    })),
    on(fetchAccountDataSuccess, (state, {myBalance}) => ({
        ...state,
        loading: false,
        accountDataLoaded: true,
        myBalance,
    })),
    on(fetchAccountDataFailed, (state, {errorMessage}) => ({
        ...state,
        loading: false,
        accountDataLoaded: false,
        errorMessage,
    })),
);


