import {AccountContractStateReducer, CollectionStateReducer} from './reducers';
import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import {ContractDataModel} from "../../models/collection.model";

export interface AppState {
  collectionState: CollectionState;
  accountContractState: AccountContractState;
}

export interface AccountContractState {
  accountDataLoaded: boolean,
  errorMessage: string | null;
  account: string | null;
  myBalance: number;
  loading: boolean;
}

export interface CollectionState {
  loaded: boolean,
  errorMessage: string | null;
  contractData: ContractDataModel | null;
  loading: boolean;
}



export const reducers: ActionReducerMap<AppState> = {
  accountContractState: AccountContractStateReducer,
  collectionState: CollectionStateReducer
};

export const metaReducers: MetaReducer<AppState>[] =  [];
