import { AppState } from '../reducers';
import { createSelector } from '@ngrx/store';

export const selectCollectionState = (state: AppState) => state.collectionState;


export const getAssetsCollection = createSelector(
    selectCollectionState,
  state => state.contractData
);

export const totalSupply = createSelector(
    selectCollectionState,
    state => state.contractData?.totalSupply
);
