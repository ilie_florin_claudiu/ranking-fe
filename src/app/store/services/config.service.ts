import {Inject, Injectable} from '@angular/core';
import {APP_CONFIG} from '../../models/constants';
import {ConfigModel} from "../../models/config.model";

@Injectable({
  providedIn: 'root',
})
export class ConfigService {
  config: ConfigModel;

  constructor(
    @Inject(APP_CONFIG) private readonly appConfig: ConfigModel
  ) {
    this.config = this.appConfig;

  }
}
