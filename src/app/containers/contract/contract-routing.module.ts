import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {ContractComponent} from "./contract.component";
import {ContractListComponent} from "./components/contract-list/contract-list.component";
import {ContractDetailComponent} from "./components/contract-detail/contract-detail.component";
import {ContractAddComponent} from "./components/contract-add/contract-add.component";
import {ContractRankComponent} from "./components/contract-rank/contract-rank.component";

const routes: Routes = [
  { path: '',
    component: ContractComponent,
    children: [
      {
        path: '',
        component: ContractListComponent,
      },
      {
        path: 'add',
        component: ContractAddComponent,
      },
      {
        path: ':contractAddress',
        component: ContractDetailComponent,
      },
      {
        path: 'rank/:contractAddress',
        component: ContractRankComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContractRoutingModule {}
