import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContractComponent } from './contract.component';
import {NftComponentComponent} from "./components/nft-component/nft-component.component";
import {ContractListComponent} from "./components/contract-list/contract-list.component";
import {ContractDetailComponent} from "./components/contract-detail/contract-detail.component";
import {LazyImgDirective} from "./components/lazi-image.directive";
import {FormsModule} from "@angular/forms";
import {InfiniteScrollModule} from "ngx-infinite-scroll";
import {ContractRoutingModule} from "./contract-routing.module";
import { ContractAddComponent } from './components/contract-add/contract-add.component';
import { ContractRankComponent } from './components/contract-rank/contract-rank.component';

@NgModule({
  declarations: [
    ContractComponent,
    NftComponentComponent,
    ContractListComponent,
    ContractDetailComponent,
    LazyImgDirective,
    ContractAddComponent,
    ContractRankComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    InfiniteScrollModule,
    ContractRoutingModule
  ]
})
export class ContractModule { }
