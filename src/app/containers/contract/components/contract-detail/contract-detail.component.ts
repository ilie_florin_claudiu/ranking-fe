import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject, takeUntil} from "rxjs";
import {CollectionState} from "../../../../store/reducers";
import {PaginationQueryModel} from "../../../../models/pagination-query.model";
import {Store} from "@ngrx/store";
import {Actions} from "@ngrx/effects";
import {ConfigService} from "../../../../store/services/config.service";
import {OpenseaService} from "../../../../service/opensea.service";
import {ContractService} from "../../../../service/contract.service";
import {HttpClient} from "@angular/common/http";
import {NetworkService} from "../../../../service/network.service";
import {selectCollectionState} from "../../../../store/selectors/selectors";
import {filter} from "rxjs/operators";
import {ActivatedRoute, ParamMap} from "@angular/router";
import {ContractDataModel, PaginatedResultModel} from "../../../../models/collection.model";

@Component({
  selector: 'app-contract-detail',
  templateUrl: './contract-detail.component.html',
  styleUrls: ['./contract-detail.component.scss']
})
export class ContractDetailComponent implements OnInit, OnDestroy {

  contractData: ContractDataModel | null = null;

  collectionState$: Observable<CollectionState>;

  collectionAddress: string | null = null;
  greenPrice = 0.3;

  resultData: PaginatedResultModel | null = null;
  isLoadingData = false;
  paginationQuery = new PaginationQueryModel();

  private destroy = new Subject<boolean>();

  constructor(
      private store: Store<any>,
      private route: ActivatedRoute,
      private actions$: Actions,
      private configService: ConfigService,
      private openseaService: OpenseaService,
      private contractService: ContractService,
      private http: HttpClient,
      private networkService: NetworkService
  ) {
    this.collectionState$ = this.store.select(selectCollectionState);
    this.collectionState$.pipe(
        takeUntil(this.destroy),
        filter(collectionState => collectionState!= null && collectionState.contractData != null)
    ).subscribe(collectionState => this.contractData = collectionState.contractData);

    this.paginationQuery.pageSize = 250;
  }

  ngOnDestroy(): void {
    this.destroy.next(true);
    this.destroy.unsubscribe();
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.collectionAddress = params.get('contractAddress');
      this.loadCollectionData();
    })
  }

  async connect() {
    await this.networkService.connect();
  }

  async loadCollectionData() {
    this.getResults(false);

    /*    if (!this.contractData?.isRevealed) {
          this.store.dispatch(getCollectionRequest({collectionAddress: this.collectionAddress}));
          this.cdr.markForCheck();
        }*/
  }


  getResults(appendResults = false): void {
    this.isLoadingData = true;
    if (appendResults === false) {
      this.resultData = null;
      this.paginationQuery = new PaginationQueryModel();
    }

    this.contractService
        .getContractDataPaginated(this.collectionAddress!, !appendResults, this.paginationQuery)
        .pipe(takeUntil(this.destroy))
        .subscribe((data: PaginatedResultModel) => {
          if (appendResults && this.resultData?.data) {
            data.data.unshift(...this.resultData.data);
          }

          this.resultData = data;
          this.isLoadingData = false;

          // Check if the screen height is bigger than the partial retrieved notifications
          setTimeout(() => {

            if (
                document.documentElement.scrollHeight -
                document.documentElement.offsetHeight ===
                0 &&
                this.resultData!.data.length < this.resultData!.totalRecords
            ) {
              this.paginationQuery.pageNumber!++;
              this.getResults(true);
            }
          });
        });
  }

  onScroll(): void {
    if (
        !this.isLoadingData &&
        this.paginationQuery.pageNumber! < this.resultData!.totalPages
    ) {
      this.paginationQuery.pageNumber!++;
      this.getResults(true);
    }
  }

  async contractDelete() {
    this.contractService.contractDelete(this.collectionAddress!);
  }

  async contractMonitor() {
    this.contractService.contractMonitor(this.collectionAddress!);
  }

  async contractUpdateRank() {
    this.contractService.contractUpdateRank(this.collectionAddress!);
  }

}
