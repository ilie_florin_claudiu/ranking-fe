import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {OpenSeaNftModel, RankResult} from "../../../../models/collection.model";
import {OpenseaService} from "../../../../service/opensea.service";
import {Subject, takeUntil} from "rxjs";
import { RateLimit } from 'async-sema'

const openSeaPublicRateLimit = RateLimit(2)

@Component({
  selector: 'app-nft-component',
  templateUrl: './nft-component.component.html',
  styleUrls: ['./nft-component.component.scss']
})
export class NftComponentComponent implements OnInit, OnDestroy {
  _rankData: RankResult | null = null;
  _openSeaData: OpenSeaNftModel | null = null;

  @Input()
  contractAddress: string | null = "";

  @Input()
  greenPrice: number = 0.0;

  @Input()
  set rankData(value: RankResult) {
    this._rankData = value;
    if (value?.tokenId) {
      //load opensea data
    }
  }

  private destroy = new Subject<boolean>();

  constructor(
      private openseaService: OpenseaService
  ) { }

  async ngOnInit(): Promise<void> {
    await openSeaPublicRateLimit();
    this.openseaService.getOpenseaDetails(this.contractAddress!, this._rankData!.tokenId).pipe(
        takeUntil(this.destroy),
    ).subscribe(
        result => {
          if (result.orders) {
            let saleOrder = result.orders.find(so => {
              return so.sale_kind ===0 &&
                  so.side ===1 &&
                  so.taker_relayer_fee === "0"
            });

            if (saleOrder)
            {
              this._rankData!.salePrice = saleOrder.current_price / 1000000000000000000;
              this._rankData!.forSale = true;

            }
          }

          this._openSeaData = result;
          this._rankData!.saleLink = result.permalink;
          this._rankData!.openseaImage = result.image_thumbnail_url;
        }
    )
  }

  ngOnDestroy(): void {
    this.destroy.next(true);
    this.destroy.unsubscribe();
  }

}
