import {Component, OnDestroy, OnInit} from '@angular/core';
import {ContractModel} from "../../../../models/collection.model";
import {PaginatedModel, PaginationQueryModel} from "../../../../models/pagination-query.model";
import {Subject, takeUntil} from "rxjs";
import {Store} from "@ngrx/store";
import {Actions} from "@ngrx/effects";
import {ConfigService} from "../../../../store/services/config.service";
import {OpenseaService} from "../../../../service/opensea.service";
import {ContractService} from "../../../../service/contract.service";
import {HttpClient} from "@angular/common/http";
import {NetworkService} from "../../../../service/network.service";

@Component({
  selector: 'app-contract-list',
  templateUrl: './contract-list.component.html',
  styleUrls: ['./contract-list.component.scss']
})
export class ContractListComponent implements OnInit, OnDestroy {

  resultData: PaginatedModel<ContractModel[]> | null = null;
  isLoadingData = false;
  paginationQuery = new PaginationQueryModel();

  private destroy = new Subject<boolean>();

  constructor( private store: Store<any>,
               private actions$: Actions,
               private configService: ConfigService,
               private openseaService: OpenseaService,
               private contractService: ContractService,
               private http: HttpClient,
               private networkService: NetworkService
  ) { }

  ngOnDestroy(): void {
    this.destroy.next(true);
    this.destroy.unsubscribe();
  }

  ngOnInit(): void {
    this.loadContracts();
  }

  async loadContracts() {
    this.getResults(false);
  }

  getResults(appendResults = false): void {
    this.isLoadingData = true;
    if (appendResults === false) {
      this.resultData = null;
      this.paginationQuery = new PaginationQueryModel();
    }

    this.contractService
        .getContractsPaginated(this.paginationQuery)
        .pipe(takeUntil(this.destroy))
        .subscribe((data: PaginatedModel<ContractModel[]>) => {
          if (appendResults && this.resultData?.data) {
            data.data.unshift(...this.resultData.data);
          }

          this.resultData = data;
          this.isLoadingData = false;

          // Check if the screen height is bigger than the partial retrieved notifications
          setTimeout(() => {

            if (
                document.documentElement.scrollHeight -
                document.documentElement.offsetHeight ===
                0 &&
                this.resultData!.data.length < this.resultData!.totalRecords
            ) {
              this.paginationQuery.pageNumber!++;
              this.getResults(true);
            }
          });
        });
  }

  onScroll(): void {
    if (
        !this.isLoadingData &&
        this.paginationQuery.pageNumber! < this.resultData!.totalPages
    ) {
      this.paginationQuery.pageNumber!++;
      this.getResults(true);
    }
  }

  async contractDelete(collectionAddress: string) {
    this.contractService.contractDelete(collectionAddress);
  }

  async contractMonitor(collectionAddress: string) {
    this.contractService.contractMonitor(collectionAddress);
  }

  async contractUpdateRank(collectionAddress: string) {
    this.contractService.contractUpdateRank(collectionAddress);
  }

  async authenticate() {
    this.networkService.connect();
  }

}
