import { Component, OnInit } from '@angular/core';
import {ContractService} from "../../../../service/contract.service";
import {ContractDataModel, ContractSearchResult} from "../../../../models/collection.model";

@Component({
  selector: 'app-contract-add',
  templateUrl: './contract-add.component.html',
  styleUrls: ['./contract-add.component.scss']
})
export class ContractAddComponent implements OnInit {
  collectionAddress: string | null = "0xea745b608b2b61987785e87279db586f328eb8fc";
  contractSearchResult: ContractSearchResult | null = null;
  result : string = "";

  constructor(
      private contractService: ContractService
  ) { }

  ngOnInit(): void {
  }

  async searchContract() {
    this.contractService.searchContract(this.collectionAddress!).subscribe(res => {
      this.contractSearchResult = res;
    });
  }

  async saveContract() {
    this.contractService.saveContract(this.collectionAddress!, this.contractSearchResult!).subscribe(res => {
      this.result = 'OK';
    });
  }

  async contractMonitor() {
    this.contractService.contractMonitor(this.collectionAddress!);
  }

  async contractUpdateRank() {
    this.contractService.contractUpdateRank(this.collectionAddress!);
  }


  async rankContract() {
    this.contractService.rankContract(this.collectionAddress!);
  }
}
