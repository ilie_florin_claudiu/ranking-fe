import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractRankComponent } from './contract-rank.component';

describe('ContractRankComponent', () => {
  let component: ContractRankComponent;
  let fixture: ComponentFixture<ContractRankComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContractRankComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractRankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
